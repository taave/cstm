﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSTM.Models
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new CSTMContext(
                serviceProvider.GetRequiredService<DbContextOptions<CSTMContext>>()))
            {
                if (context.Ticket.Any())
                {
                    return;
                }

                context.Ticket.AddRange(
                    new Ticket
                    {
                        Id = 1,
                        Title = "Test 1",
                        Description = "Description of test 1. The ticket was added 5 hours ago and is due in 3 hours.",
                        Submitted = DateTime.Now.AddHours(-5),
                        Deadline = DateTime.Now.AddHours(3),
                        Status = "Open",
                        CloseDate = null
                    },
                        new Ticket
                        {
                            Id = 2,
                            Title = "Test 2",
                            Description = "This ticket was added 27 minutes ago and is due in 54 minutes.",
                            Submitted = DateTime.Now.AddMinutes(-27),
                            Deadline = DateTime.Now.AddMinutes(54),
                            Status = "Open",
                            CloseDate = null
                        },
                        new Ticket
                        {
                            Id = 3,
                            Title = "Something important",
                            Description = "This ticket was added 2 days ago and is due in 3 days.",
                            Submitted = DateTime.Now.AddDays(-2),
                            Deadline = DateTime.Now.AddDays(3),
                            Status = "Open",
                            CloseDate = null
                        },
                        new Ticket
                        {
                            Id = 4,
                            Title = "Something that is overdue",
                            Description = "For some reason it is still open.",
                            Submitted = DateTime.Now.AddDays(-5),
                            Deadline = DateTime.Now.AddMinutes(-17),
                            Status = "Open",
                            CloseDate = null
                        },
                        new Ticket
                        {
                            Id = 5,
                            Title = "UI issue",
                            Description = "This issue is already closed and cannot be opened or deleted.",
                            Submitted = DateTime.Now.AddDays(-5),
                            Deadline = DateTime.Now.AddMinutes(17),
                            Status = "Closed",
                            CloseDate = DateTime.Now.AddDays(-1)
                        }
                    );

                context.SaveChanges();
            }
        }
    }
}
