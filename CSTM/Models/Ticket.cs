﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CSTM.Models
{
    public class Ticket
    {
        public int Id { get; set; }

        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
        
        [DisplayName("Created")]
        public DateTime Submitted { get; set; }

        [DisplayName("Due")]
        [FutureDateValidation()]
        public DateTime Deadline { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Closed on")]
        public DateTime? CloseDate { get; set; }


        [AttributeUsage(AttributeTargets.Property)]
        public class FutureDateValidation : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                DateTime inputDate = (DateTime)value;

                if (inputDate > DateTime.Now)
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Please set a due date in the future.");
                }
            }
        }
        
    }
}
